#include "utils.h"

RandGenerator::RandGenerator(int min, int max, uint32_t seed)
: _gen(seed)
, _dist(min, max)
{}

int RandGenerator::Rand() 
{
	return _dist(_gen);
}
