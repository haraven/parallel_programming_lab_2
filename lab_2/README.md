# Matrix multiplication

---

### Overview

Divide a simple task between threads. The task can easily be divided in sub-tasks requiring no cooperation at all. See the effects of false sharing, and the costs of creating threads and of switching between threads.

### Requirement 1

Write two problems: one for computing the sum of two matrices, the other for computing the product of two matrices. Divide the task between a configurable number of threads (going from 1 to the number of elements in the resulting matrix). See the effects on the execution time.

### Requirement 2
Same as requirement 1, but with 3 diffferent methods for handling multithreading: std::threads, std::async, and a custom ThreadPool class. Compare execution times.

### Requirement 3
Threeway matrix multiplication (suppose matrices are A, B, and C). R1 = A * B is handled by one set of threads, and R2 = R1 * C is handled by another set of threads. R2 computations start as soon as slices of R1 start coming in.

### Libraries used:
* [Boost]
* [EasyLoggingPP]
* [ThreadPool]

###### **Babeș-Bolyai University, 2016**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job)
[Boost]: <http://www.boost.org/>
[EasyLoggingPP]: <https://github.com/easylogging/easyloggingpp>
[ThreadPool]: <https://github.com/progschj/ThreadPool>