#include "matrix_helper.h"
#include <vector>
#include <thread>
#include <threadpool.h>

#define ELPP_THREAD_SAFE
#define ELPP_STL_LOGGING
#include "logger.hpp"

std::mutex log_mutex;

void hrv::MatrixHelper::ThreadedPerformOperation(OperationType op, const size_t& thread_count)
{
	std::function<void(ThreadArgs)> thread_fn = op == OPERATION_ADD ? ThreadAdd : ThreadMultiply;
	_thread_count = thread_count;

	std::vector<std::thread> threads;
	threads.reserve(thread_count);

    size_t arows_times_bcols = _A.Rows() * _B.Columns();
    size_t from = 0,
        to      = 0,
        step    = (arows_times_bcols) / thread_count,
        left    = (arows_times_bcols) % thread_count;

    _R.Clear();
    for (size_t i = 0; i < thread_count; ++i)
    {
        from = to;
        to += step;
        if (left)
        {
            --left;
            ++to;
        }

        threads.push_back(std::thread(thread_fn, ThreadArgs(std::ref(*this), from, to)));
    }
    
    for (auto&& thread : threads)
        thread.join();
}

void hrv::MatrixHelper::AsyncPerformOperation(OperationType op, const size_t & thread_count)
{
    std::function<void(ThreadArgs)> thread_fn = op == OPERATION_ADD ? ThreadAdd : ThreadMultiply;
    _thread_count = thread_count;

    std::vector<std::future<void>> results;
    results.reserve(thread_count);

    size_t arows_times_bcols = _A.Rows() * _B.Columns();
    size_t from = 0,
        to      = 0,
        step    = (arows_times_bcols) / thread_count,
        left    = (arows_times_bcols) % thread_count;

    _R.Clear();
    for (size_t i = 0; i < thread_count; ++i)
    {
        from = to;
        to += step;
        if (left)
        {
            --left;
            ++to;
        }

        results.push_back(std::async(thread_fn, ThreadArgs(std::ref(*this), from, to)));
    }

    for (auto&& res : results)
        res.get();
}

void hrv::MatrixHelper::PooledPerformOperation(OperationType op, const size_t & thread_count)
{
    std::function<void(ThreadArgs)> thread_fn = op == OPERATION_ADD ? ThreadAdd : ThreadMultiply;
    _thread_count = thread_count;

    ThreadPool pool(thread_count);
    std::vector<std::future<void>> results;

    size_t arows_times_bcols = _A.Rows() * _B.Columns();
    size_t from = 0,
        to      = 0,
        step    = (arows_times_bcols) / thread_count,
        left    = (arows_times_bcols) % thread_count;

    _R.Clear();
    for (size_t i = 0; i < thread_count; ++i)
    {
        from = to;
        to += step;
        if (left)
        {
            --left;
            ++to;
        }

        results.emplace_back(pool.enqueue([&thread_fn, this, &from, &to]
        {
            thread_fn(ThreadArgs(std::ref(*this), from, to));
        }));
    }

    for (auto&& res : results)
        res.get();
}

void hrv::MatrixHelper::ThreewayPerformOperation(OperationType op, const size_t & thread_count)
{
    //std::function<void(ThreadArgs)> thread_fn = op == OPERATION_ADD ? ThreadAdd : ThreadMultiply;
    std::function<ThreewayThreadArgs(ThreadArgs)> thread_first_op_fn = ThreewayFirstThreadMultiply;
    _thread_count = thread_count;

    std::vector<std::shared_future<ThreewayThreadArgs>> results;
    results.reserve(thread_count);

    size_t arows_times_bcols = _A.Rows() * _B.Columns();
    size_t from = 0,
        to = 0,
        step = (arows_times_bcols) / thread_count,
        left = (arows_times_bcols) % thread_count;

    _R_3way.Clear();
    for (size_t i = 0; i < thread_count; ++i)
    {
        from = to;
        to += step;
        if (left)
        {
            --left;
            ++to;
        }

        results.push_back(std::async(thread_first_op_fn, ThreadArgs(std::ref(*this), from, to)).share());
    }

    std::function<ThreewayThreadArgs(std::shared_future<ThreewayThreadArgs>)> thread_second_op_fn = ThreewaySecondThreadMultiply;
    std::vector<std::future<ThreewayThreadArgs>> second_multiplication_results;
    second_multiplication_results.reserve(thread_count);
    for (auto&& res : results)
        //second_multiplication_results.push_back(std::async(thread_fn, res.get()));
        second_multiplication_results.push_back(std::async(thread_second_op_fn, res));

    for (auto&& res : second_multiplication_results)
        res.get();
}

hrv::MatrixHelper::MatrixHelper(Matrix & A, Matrix & B)
: _A(A)
, _B(B)
, _C(Matrix())
, _R(A.Columns(), B.Rows(), "R")
{}

hrv::MatrixHelper::MatrixHelper(Matrix & A, Matrix & B, Matrix & C)
: _A(A)
, _B(B)
, _C(C)
, _R(A.Columns(), B.Rows(), "R")
, _R_3way(_R.Columns(), C.Rows(), "R_threeway")
{}

hrv::MatrixHelper & hrv::MatrixHelper::ThreadedMultiply(const size_t & thread_count)
{
	if (_A.Columns() != _B.Rows() && _A.Rows() != _A.Columns() && _B.Rows() != _B.Columns())
	{
		log_mutex.lock();
		LOG(ERROR) << "Cannot perform matrix multiplication: matrix size mismatch.\nMatrix A is " 
			<< _A.Rows() << " x " << _A.Columns()
			<< "\nMatrix B is " << _B.Rows() << " x " << _B.Columns();
		log_mutex.unlock();
	}

	ThreadedPerformOperation(OPERATION_MULTIPLY, thread_count);

	return *this;
}

hrv::MatrixHelper & hrv::MatrixHelper::ThreadedAdd(const size_t & thread_count)
{
	ThreadedPerformOperation(OPERATION_ADD, thread_count);

	return *this;
}

hrv::MatrixHelper & hrv::MatrixHelper::AsyncMultiply(const size_t & thread_count)
{
    AsyncPerformOperation(OPERATION_MULTIPLY, thread_count);

    return *this;
}

hrv::MatrixHelper & hrv::MatrixHelper::AsyncAdd(const size_t & thread_count)
{
    AsyncPerformOperation(OPERATION_ADD, thread_count);

    return *this;
}

hrv::MatrixHelper & hrv::MatrixHelper::PooledMultiply(const size_t & thread_count)
{
    PooledPerformOperation(OPERATION_MULTIPLY, thread_count);

    return *this;
}

hrv::MatrixHelper & hrv::MatrixHelper::PooledAdd(const size_t & thread_count)
{
    PooledPerformOperation(OPERATION_ADD, thread_count);

    return *this;
}

hrv::MatrixHelper & hrv::MatrixHelper::SetA(Matrix & A)
{
	_A = A;

	return *this;
}

hrv::MatrixHelper & hrv::MatrixHelper::SetB(Matrix& B)
{
	_B = B;

	return *this;
}

Matrix & hrv::MatrixHelper::ThreewayMultiply(const size_t & thread_count)
{
    ThreewayPerformOperation(OPERATION_MULTIPLY, thread_count);

    return _R_3way;
}

void hrv::ThreadMultiply(ThreadArgs args)
{
	Matrix& A = args.helper._A;
	Matrix& B = args.helper._B;
	Matrix& C = args.helper._R;

    const size_t& size = B.Columns();
    const size_t& a_rowcount = A.Rows();
    const size_t& b_rowcount = B.Rows();
    const size_t& from = args.from;
    const size_t& to = args.to;

	//log_mutex.lock();
	//LOG(INFO) << "Beginning multiplication computation, on rows " << from << " through " << to << std::endl;
	//log_mutex.unlock();
    for (size_t crt = from; crt < to; ++crt)
    {
        int i = crt / size;
        int j = crt % size;

        C[i % a_rowcount][j] = 0;
        for (size_t k = 0; k < b_rowcount; ++k)
            C[i][j] += A[i][k] * B[k][j];
    }

	//log_mutex.lock();
	//LOG(INFO) << "Ending multiplication computation, on rows " << from << " through " << to << std::endl;
	//log_mutex.unlock();
}

void hrv::ThreadAdd(ThreadArgs args)
{
	Matrix& A = args.helper._A;
	Matrix& B = args.helper._B;
	Matrix& C = args.helper._R;

	const size_t& size = B.Columns();
    const size_t& from = args.from;
    const size_t& to   = args.to;

    for (size_t crt = from; crt < to; crt++)
    {
        size_t i = crt / size;
        size_t j = crt % size;

        C[i][j] = A[i][j] + B[i][j];
    }

	/*log_mutex.lock();
	LOG(INFO) << "Beginning addition computation, on rows " << from << " through " << to << std::endl;
	log_mutex.unlock();*/
	//for (size_t i = from; i < to; ++i)
	//	for (size_t j = 0; j < size; ++j)
	//		C[i][j] = A[i][j] + B[i][j];

	/*log_mutex.lock();
	LOG(INFO) << "Ending addition computation, on rows " << from << " through " << to << std::endl;
	log_mutex.unlock();*/
}

hrv::ThreewayThreadArgs hrv::ThreewayFirstThreadMultiply(ThreadArgs args)
{
    Matrix& A = args.helper._A;
    Matrix& B = args.helper._B;
    Matrix& R = args.helper._R;

    const size_t& size = B.Columns();
    const size_t& a_rowcount = A.Rows();
    const size_t& b_rowcount = B.Rows();
    const size_t& from = args.from;
    const size_t& to = args.to;

    //log_mutex.lock();
    //LOG(INFO) << "Beginning multiplication computation, on rows " << from << " through " << to << std::endl;
    //log_mutex.unlock();
    for (size_t crt = from; crt < to; ++crt)
    {
        int i = crt / size;
        int j = crt % size;

        R[i % a_rowcount][j] = 0;
        for (size_t k = 0; k < b_rowcount; ++k)
            R[i][j] += A[i][k] * B[k][j];
    }
    
    return ThreewayThreadArgs(&args.helper, from, to);

    //log_mutex.lock();
    //LOG(INFO) << "Ending multiplication computation, on rows " << from << " through " << to << std::endl;
    //log_mutex.unlock();

}

hrv::ThreewayThreadArgs hrv::ThreewaySecondThreadMultiply(std::shared_future<hrv::ThreewayThreadArgs> arg)
{
    ThreewayThreadArgs args = arg.get();

    Matrix& A = std::get<0>(args)->_R;
    Matrix& B = std::get<0>(args)->_C;
    Matrix& R = std::get<0>(args)->_R_3way;

    const size_t& size = B.Columns();
    const size_t& a_rowcount = A.Rows();
    const size_t& b_rowcount = B.Rows();
    const size_t& from = std::get<1>(args);
    const size_t& to = std::get<2>(args);

    //log_mutex.lock();
    //LOG(INFO) << "Beginning multiplication computation, on rows " << from << " through " << to << std::endl;
    //log_mutex.unlock();
    for (size_t crt = from; crt < to; ++crt)
    {
        int i = crt / size;
        int j = crt % size;

        R[i % a_rowcount][j] = 0;
        for (size_t k = 0; k < b_rowcount; ++k)
            R[i][j] += A[i][k] * B[k][j];
    }

    return args;

    //log_mutex.lock();
    //LOG(INFO) << "Ending multiplication computation, on rows " << from << " through " << to << std::endl;
    //log_mutex.unlock();
}