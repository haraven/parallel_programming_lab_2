#pragma once

#include <string>
#include "utils.h"

class Matrix
{
private:
	const uint32_t RAND_GEN_MAX = 10;

	RandGenerator& GetRandomGenerator();
	void AllocateMatrix();
public:
	class MatrixRow 
	{
		friend class Matrix;
	private:
		MatrixRow(Matrix &parent, int row);
	public:
		int& operator[](size_t col)
		{
			return _parent._m[_row][col];
		}

        const int& operator[](size_t col) const
        {
            return _parent._m[_row][col];
        }
	private:
		Matrix& _parent;
		size_t _row;
	};

	Matrix(const std::string& name = "");
	Matrix(size_t rows, size_t cols, const std::string& name = "");

	Matrix& Init(const std::string& filename);
	Matrix& Init();
    Matrix& Clear();

    int& At(size_t i, size_t j);
    const int& At(size_t i, size_t j) const;

	const size_t& Rows() const { return _rows; }
	const size_t& Columns() const { return _columns; }

	Matrix::MatrixRow operator[](size_t row);
	Matrix& operator=(const Matrix& ot);

	Matrix& ShallowCopy(const Matrix& ot);

	~Matrix();
private:
	std::string _name;
	int** _m;
	size_t _rows;
	size_t _columns;
};

