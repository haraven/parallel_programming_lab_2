#pragma once

#include "matrix.h"
#include <mutex>
#include <future>
#include <tuple>

namespace hrv
{
	class MatrixHelper;

    typedef std::tuple<MatrixHelper*, size_t, size_t> ThreewayThreadArgs;

	class ThreadArgs
	{
	public:
		ThreadArgs(MatrixHelper& helper, size_t from, size_t to)
			: helper(helper)
			, from(from)
            , to(to)
		{}

		MatrixHelper& helper;
        size_t from;
        size_t to;
	};

	class MatrixHelper
	{
	private:
		friend void ThreadMultiply(ThreadArgs args);
		friend void ThreadAdd(ThreadArgs args);
        friend hrv::ThreewayThreadArgs ThreewayFirstThreadMultiply(hrv::ThreadArgs args);
        friend hrv::ThreewayThreadArgs ThreewaySecondThreadMultiply(std::shared_future<hrv::ThreewayThreadArgs> arg);

		enum OperationType
		{
			OPERATION_ADD = 1,
			OPERATION_MULTIPLY = 2
		};

		void ThreadedPerformOperation(OperationType op, const size_t& thread_count);
        void AsyncPerformOperation(OperationType op, const size_t& thread_count);
        void PooledPerformOperation(OperationType op, const size_t& thread_count);
        void ThreewayPerformOperation(OperationType op, const size_t& thread_count);
	public:
		MatrixHelper(Matrix& A, Matrix& B);
        MatrixHelper(Matrix& A, Matrix& B, Matrix& C);

		MatrixHelper& ThreadedMultiply(const size_t& thread_count);
		MatrixHelper& ThreadedAdd(const size_t& thread_count);
        MatrixHelper& AsyncMultiply(const size_t& thread_count);
        MatrixHelper& AsyncAdd(const size_t& thread_count);
        MatrixHelper& PooledMultiply(const size_t& thread_count);
        MatrixHelper& PooledAdd(const size_t& thread_count);

		MatrixHelper& SetA(Matrix& A);
		MatrixHelper& SetB(Matrix& B);

        // undefined behavior if called without a 3rd matrix passed to the constructor
        Matrix& ThreewayMultiply(const size_t& thread_count);

		const Matrix& GetResult() const { return _R; }
        const Matrix& GetThreewayResult() const { return _R_3way; }
	private:
		Matrix& _A;
		Matrix& _B;
        Matrix& _C;
		Matrix _R;
        Matrix _R_3way;
		size_t _thread_count;
	};
}