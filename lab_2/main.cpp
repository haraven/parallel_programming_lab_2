#include <sstream>
#include <fstream>
#include <boost/thread/thread.hpp>
#include <boost/timer/timer.hpp>
#include "matrix.h"
#include "matrix_helper.h"

#include "logger.hpp"
INITIALIZE_EASYLOGGINGPP

enum RetCodes
{
	SUCCESS = 0,
	FAILURE = -1
};

int main()
{
    std::ofstream out("res.csv");
    el::Loggers::configureFromGlobal("logging.conf");

	std::stringstream ss;
	size_t rowcount = 100;
	size_t colcount = 100;
	size_t thread_count = rowcount * colcount;

	Matrix A(rowcount, colcount, "A"), B(rowcount, colcount, "B"), C(rowcount, colcount, "C");
	A.Init();
	B.Init();
    C.Init();

    hrv::MatrixHelper helper(A, B, C);

    for (size_t i = 0; i < thread_count; ++i)
    {
        boost::timer::cpu_timer timer;
        helper.ThreewayMultiply(i + 1);
        timer.stop();
        boost::timer::cpu_times cpu_times = timer.elapsed();
        LOG(INFO) << "For " << i + 1 <<  (i > 0 ? " threads" : " thread") << ", time elapsed: " << cpu_times.wall << " nanoseconds";
        out << i + 1 << ", " << cpu_times.wall << std::endl;
    }

    out.close();

	//Matrix A("A"), B("B");
	//A.Init("example_1.txt");
	//B.Init("example_1.txt");

	//for (size_t i = 0; i < A.Rows(); ++i)
	//{
	//	for (size_t j = 0; j < A.Columns(); ++j)
	//		ss << A[i][j] << " ";
	//	ss << std::endl;
	//}
	//LOG(INFO) << "A:\n" << ss.str();

	//ss.str(std::string());
	//for (size_t i = 0; i < B.Rows(); ++i)
	//{
	//	for (size_t j = 0; j < B.Columns(); ++j)
	//		ss << B[i][j] << " ";
	//	ss << std::endl;
	//}
	//LOG(INFO) << "B:\n" << ss.str();
	//ss.str(std::string());

 //   ss.str(std::string());
 //   for (size_t i = 0; i < C.Rows(); ++i)
 //   {
 //       for (size_t j = 0; j < C.Columns(); ++j)
 //           ss << C[i][j] << " ";
 //       ss << std::endl;
 //   }
 //   LOG(INFO) << "C:\n" << ss.str();
 //   ss.str(std::string());

 //   for (size_t i = 0; i < R.Rows(); ++i)
 //   {
 //       for (size_t j = 0; j < R.Columns(); ++j)
 //           ss << R.At(i, j) << " ";
 //       ss << std::endl;
 //   }
 //   LOG(INFO) << "R (first multiplication):\n" << ss.str();
 //   ss.str(std::string());

 //   for (size_t i = 0; i < R_3way.Rows(); ++i)
 //   {
 //       for (size_t j = 0; j < R_3way.Columns(); ++j)
 //           ss << R_3way.At(i, j) << " ";
 //       ss << std::endl;
 //   }
 //   LOG(INFO) << "R (second multiplication):\n" << ss.str();
 //   ss.str(std::string());




    //hrv::MatrixHelper helper(A, B);
    //for (size_t i = 0; i < thread_count; ++i)
    //{
    //    std::chrono::milliseconds start = std::chrono::duration_cast<std::chrono::milliseconds>
    //        (
    //            std::chrono::system_clock::now().time_since_epoch()
    //        );
    //    helper.ThreadedAdd(i + 1);
    //    std::chrono::milliseconds end = std::chrono::duration_cast<std::chrono::milliseconds>
    //        (
    //            std::chrono::system_clock::now().time_since_epoch()
    //        );
    //    double threaded_ms = static_cast<double>((end - start).count());

    //    start = end;
    //    helper.AsyncAdd(i + 1);
    //    end = std::chrono::duration_cast<std::chrono::milliseconds>
    //        (
    //            std::chrono::system_clock::now().time_since_epoch()
    //        );
    //    double async_ms = static_cast<double>((end - start).count());

    //    start = end;
    //    helper.PooledAdd(i + 1);
    //    end = std::chrono::duration_cast<std::chrono::milliseconds>
    //        (
    //            std::chrono::system_clock::now().time_since_epoch()
    //        );

    //    LOG(INFO) << "For " << i + 1 << " thread(s)" << std::endl
    //        << "\tRegular threaded execution finished in " << threaded_ms << " ms" << std::endl
    //        << "\tstd::async execution finished in " << async_ms << " ms" << std::endl
    //        << "\tPooled execution finished in " << static_cast<double>((end - start).count()) << " ms" << std::endl;
    //}

	return SUCCESS;
}
