#pragma once

#include <boost/uuid/uuid_generators.hpp>

class RandGenerator
{
public:
	RandGenerator(int min, int max, uint32_t seed = static_cast<const uint32_t>(time(NULL)));

	int Rand();
private:
	boost::random::mt19937 _gen;
	boost::random::uniform_int_distribution<> _dist;
};

template<typename T> void safe_delete(T*& a)
{
	if (a != nullptr)
	{
		delete a;
		a = nullptr;
	}
}

template<typename T> void safe_delete_arr(T*& a)
{
	if (a != nullptr)
	{
		delete[] a;
		a = nullptr;
	}
}