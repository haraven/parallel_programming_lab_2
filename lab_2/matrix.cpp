#include "matrix.h"
#include <fstream>
#include "logger.hpp"

RandGenerator & Matrix::GetRandomGenerator()
{
	static RandGenerator rand(0, RAND_GEN_MAX);

	return rand;
}

void Matrix::AllocateMatrix()
{
	_m = new int*[_rows];
	_m[0] = new int[_rows * _columns];
	for (size_t i = 1; i < _rows; ++i)
		_m[i] = _m[0] + i * _columns;
}

Matrix::Matrix(const std::string& name)
: _rows(0)
, _columns(0)
, _name(name)
{}

Matrix::Matrix(size_t rows, size_t cols, const std::string& name)
: _rows(rows)
, _columns(cols)
, _name(name)
{
	if (_rows && _columns)
		AllocateMatrix();
}

Matrix & Matrix::Init(const std::string & filename)
{
	std::ifstream file(filename);
	file >> _rows >> _columns;
	if (_rows && _columns)
	{
		AllocateMatrix();

		for (size_t i = 0; i < _rows; ++i)
			for (size_t j = 0; j < _columns; ++j)
				file >> _m[i][j];
	}

	file.close();

	return *this;
}

Matrix & Matrix::Init()
{
	RandGenerator& rand = GetRandomGenerator();
	for (size_t i = 0; i < _rows; ++i)
		for (size_t j = 0; j < _columns; ++j)
			_m[i][j] = rand.Rand();

	return *this;
}

Matrix & Matrix::Clear()
{
    ZeroMemory(_m[0], sizeof(_m[0]));

    return *this;
}

int& Matrix::At(size_t i, size_t j)
{
    return _m[i][j];
}

const int & Matrix::At(size_t i, size_t j) const
{
    return _m[i][j];
}

Matrix::MatrixRow Matrix::operator[](size_t row)
{
	return MatrixRow(*this, row);
}

Matrix & Matrix::operator=(const Matrix & ot)
{
	_rows = ot._rows;
	_columns = ot._columns;
	_m = ot._m;

	return *this;
}

Matrix & Matrix::ShallowCopy(const Matrix & ot)
{
	return this->operator=(ot);
}

Matrix::~Matrix()
{
    if (_rows) safe_delete_arr(_m[0]);
    safe_delete_arr(_m);
}

Matrix::MatrixRow::MatrixRow(Matrix & parent, int row)
: _parent(parent)
, _row(row)
{}
